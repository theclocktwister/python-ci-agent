# Python CI Agent

A CI/CD agent that performs coverage analysis, unit tests and updates your badges

## Versions

### Versioning scheme
The way we release and tag versions is quite simple: A version consists of 3 digits:
`v<major>.<minor>.<patch>` like `v1.2.1`.
- `major` - Changes if a version is no more compatible to its predecessor
- `minor` - Changes if a new feature is implemented
- `patch` - Changes if a feature is improved / bug is fixed

### Features

| Feature name      | Introduced    | Description
|-------------------|---------------|-------------|
| Lint check        | `v2.0.0`      | Performs PyLint analysis
| Badge creation    | `v1.1.0`      | Generates badges for succeeded tests
| Unit tests        | `v1.0.0`      | Run unit tests and collect report (HTML)
| Unit test PDF     | `v1.0.0`      | Renders the unit tests results as PDF


## Script parameters

### Functional parameters
These parameters run specific functins and tasks on your code. You must use at least one,
but can use any number of them in any desired order.

_Note: The order in which the arguments are given will not have an impact on the actual
order of execution._

| Argument | Description
|----------|-------------------------------------------------
| `--test` | Runs unit tests & coverage for your code (creates badges).
| `--lint` | Performs a lint check on your code using [PyLint](https://www.pylint.org/).

### Additional parameters

| Argument    | Value     | Default    | Example           |  Description
|-------------|-----------|------------|-------------------|-------------------------------------------------
| `--src`     | directory | `./`       | `--src=test/src`  | Your code source directory.
| `--out`     | directory | `./output` | `--out=/out_dir`  | The output folder where PDFs and badges are saved to.
| `--license` | string    |            | `--license=GPLv3` | Creates an additional badge with the license name.


## GitLab CI/CD configuration block

### Run unit tests and coverage
```yaml
Unit Tests:
  image: registry.gitlab.com/theclocktwister/python-ci-agent:latest
  stage: deploy
  script:
    - pyci --test --out=/output --src=<your_code_fodler> # will produce test results in /output
  artifacts:
    name: "Unit Test Reports"
    paths:
      - /output/*
    when: always  # artifacts must be uploaded even if the unit tests fail
    expire_in: 1 week
```

### Run unit tests and push new badges
```yaml
before_script: # login to git to allow pushes
  - git remote set-url origin https://gitlab-ci-token:$PERSONAL_ACCESS_TOKEN@gitlab.com/$CI_PROJECT_PATH.git
  - git config --global user.email 'myuser@mydomain.com'
  - git config --global user.name 'CI Worker'

Unit Tests:
  # Updates the badges for README.md and push to `badges`
  # based on the results of unit test and coverage report
  image: registry.gitlab.com/theclocktwister/python-ci-agent:latest
  stage: deploy
  script:
    - pyci <...>
    - git checkout badges
    - git pull origin badges
    - mkdir -p badges/ && cp /output/badges/* badges/ # copy generated badges into repo
    - git add badges/*
  after_script:
    - git commit -m "Update from unit tests"
    - git push origin HEAD:badges
  allow_failure: true
  rules:
    - if: $CI_COMMIT_REF_NAME != "badges" # prevent CI loop
  artifacts:
    name: "Unit Test Reports"
    paths:
      - /output/*
    when: always  # artifacts must be uploaded even if the unit tests fail
    expire_in: 1 week
```
