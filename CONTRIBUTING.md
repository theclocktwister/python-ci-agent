# Contributing
When contributing to this repository, please first discuss the change you wish
to make via issue before submitting a merge request.

## Code Style
Please assure that cour contribution complies to the following code style before
asking to merge a merge request (you may just set it to WIP and correct code style). 

### Required
- All variable names must be in English
- Code documentation must be in English
- All code must be readable (no binaries)

### Optional
- [PEP8 naming convention](https://www.python.org/dev/peps/pep-0008/#prescriptive-naming-conventions)
- A blank line at the end of each file
- Docstrings for every class, method and function 
- Unit tests for good coverage
