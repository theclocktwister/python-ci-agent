"""
in order to test the correctness of the generated output, we've set-up a
codebase with a few errors and see if the generated badges are showing
the corresponding coverage score, lint score and unit test score.

# generator.py --src=test/src --out=output --test


"""
from sys import argv
from os.path import join


def get_badge_value(path: str):
    with open(path, "r") as f:
        lines = f.readlines()
        lines.reverse()
        for line in lines:
            line = line.strip()
            if line.endswith("</text>"):
                line = line.rstrip("</text>")
                return line.split(">")[-1]


output_dir = "/output/"
for arg in argv:
    if arg.startswith("--out="):
        output_dir = arg.split("=")[1]

results = {
    "badges/unittest.svg": "3/4",
    "badges/coverage.svg": "89%",
    "badges/lint.svg": "38%",
    "badges/license.svg": "GPLv3"
}

if __name__ == '__main__':
    success = True

    print("\nexpected results:")
    for path, expected_result in results.items():
        print("{:<20} {:>10}".format(path, expected_result))

    for path, expected_result in results.items():
        result = get_badge_value(join(output_dir, path))
        if result != expected_result:
            print(f"Validation of {path} failed.")
            print(f"Expected: {expected_result}, actual: {result}")
            success = False

    if not success:
        exit(1)
    print("\nOutput matches defined expectations.")
