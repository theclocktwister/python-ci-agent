from functions import subtract


def test_subtract_int():
    """ Will succeed """
    assert subtract(4, 2) == 2


def test_subtract_str():
    """ Will fail with exception """
    assert "34" - "4" == "3"
