"""
Just a bunch of functions
"""


def add(a, b):
    """ This is a doc-string"""
    return a + b


def subtract(a, b):
    """ This is a doc-string"""
    return a - b


def multiply(a, b):
    return a * b


def divide(a, b):
    """ Is not covered """
    return a / b
