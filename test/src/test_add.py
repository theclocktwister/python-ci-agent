from functions import add


def test_add_int():
    """ Will succeed """
    assert add(3, 4) == 7


def test_add_str():
    """ Will succeed """
    assert add("3", "4") == "34"
