#!/usr/bin/env python

import anybadge
import functools
import pylint.epylint as linter
from os.path import join
import os
from sys import argv


class C:
    blue = "#1081c1"
    green = "#40ba12"
    lime = "#89b702"
    yellow = "yellow"
    orange = "orange"
    red = "red"


class Badge:
    """
    Functions decorated with this decorator can access a local instance
    of `Badge` and manipulate color, label, value and padding. At the end
    of their execution, they will create a label with the defined props.
    If you want to execute all functions this decorator was used in, simply
    call `Badge.render_all()`.
    """

    _functions = []
    label = ""
    value = ""
    color = "lightgrey"
    padding = 0.2

    def __init__(self, path: str):
        self._path = path

    def __call__(self, f):
        """
        Registers a function to be called on render().
        """

        @functools.wraps(f)
        def evaluation_function(*args, **kwargs):
            f.__globals__[self.__class__.__name__] = self
            f(*args, **kwargs)
            badge = anybadge.Badge(
                label=self.label,
                value=self.value,
                default_color=self.color,
                num_padding_chars=self.padding
            )
            try:
                os.remove(self._path)
            except FileNotFoundError:
                pass
            badge.write_badge(self._path)

        self._functions.append(evaluation_function)
        return evaluation_function

    @classmethod
    def render_all(cls):
        for f in cls._functions:
            f()

    @staticmethod
    def color_threshold(value: float):
        if value < 50:
            return C.red
        if value < 60:
            return C.orange
        if value < 70:
            return C.yellow
        if value < 80:
            return C.lime
        if value < 60:
            return C.green


output_dir = "/output/"
for arg in argv:
    if arg.startswith("--out="):
        output_dir = arg.split("=")[1]

source_dir = "."
for arg in argv:
    if arg.startswith("--src="):
        source_dir = arg.split("=")[1]

# safe create output folder
os.makedirs(join(output_dir, "badges"), exist_ok=True)


@Badge(join(output_dir, "badges/license.svg"))
def license():
    for arg in argv:
        if arg.startswith("--license="):
            print("yee")
            Badge.color = C.blue
            Badge.label = "license"
            Badge.value = arg.split("=")[1]


@Badge(join(output_dir, "badges/unittest.svg"))
def unittest():
    def parse_test_results(file: str):
        """
        Parses the pytest output and returns tuple of passed tests and tests performed
        """
        passed, failed = 0, 0
        with open(file, "r") as f:
            lines = f.readlines()
            lines.reverse()  # start from behind, save time
            for line in lines:
                line = line.strip()
                if all([line.startswith("==="), line.endswith("==="), "in" in line, ("passed" in line or "failed" in line)]):
                    line = line.strip("=").strip()
                    passed = int(line.split("passed")[0].strip().split(" ")[-1]) if "passed" in line else 0
                    failed = int(line.split("failed")[0].strip().split(" ")[-1]) if "failed" in line else 0
                    break

        return passed, failed + passed

    # Run unit tests and convert to pdf
    os.system(f"pytest"
              f" -vv"
              f" --cov-report term-missing"
              f" --html={join(output_dir, 'unittest_report.html')}"
              f" --cov={source_dir} {source_dir}"
              f" >{join(output_dir, 'coverage_report.txt')}"
              )
    os.system(f"wkhtmltopdf"
              f" --enable-local-file-access"
              f" {join(output_dir, 'unittest_report.html')}"
              f" {join(output_dir, 'unittest_report.pdf')}"
              )

    passed, performed = parse_test_results(join(output_dir, "coverage_report.txt"))
    Badge.label = "unit tests"
    Badge.value = "{}/{}".format(passed, performed)
    if performed:  # if performed == 0, use grey default color
        Badge.color = Badge.color_threshold(passed / performed * 100)

    coverage()


@Badge(join(output_dir, "badges/coverage.svg"))
def coverage():
    with open(join(output_dir, "coverage_report.txt"), "r") as f:
        lines = f.readlines()
        lines.reverse()
        for line in lines:
            line = line.strip().split(" ")
            while '' in line: line.remove('')
            # line is now free of spaces
            try:
                int(line[1])
                int(line[2])
                if line[3].endswith("%"):
                    int(line[3][:-1])
                else:
                    raise ValueError()

                print("Found unit test results.")

                Badge.value = "{}%".format(line[-1][:-1])
                Badge.label = "coverage"
                Badge.color = Badge.color_threshold(int(line[-1][:-1]))
                return Badge.value

            except (ValueError, IndexError):
                pass
        else:
            print("No unit test results found :(")
            exit(1)


@Badge(join(output_dir, "badges/lint.svg"))
def lint(directory: str = source_dir, verbose: bool = True, max_line_length: int = 140):
    results = linter.py_run(
        "{} --max-line-length={}".format(directory, max_line_length),
        return_std=True
    )[0].getvalue()
    with open(join(output_dir, "lint_log.txt"), "a")as f:
        f.write(results)
    for line in results.split("\n"):
        if verbose: print(line)
        if "Your code has been rated at" in line:
            current_score = float(line.split(" (")[0].split("at ")[1].split("/")[0])
            Badge.label = "lint score"
            Badge.value = "{}%".format(int(current_score * 10))
            Badge.color = Badge.color_threshold(int(current_score * 10))
            return current_score


def show_help():
    print("""
    AVAILABLE TESTS:
    --test              Run unit test, coverage and create badge
    --lint              Run lint check and create badge
    
    ADDITIONAL BADGES:
    --license=<str>     Specify a license to create a license badge
    
    DIRECTORIES:
    --src=<str>         Specify a code source directory, default: '.'
    --out=<str>         Specify an output directory, default: '/output'
    """)


actions = {
    "--license=": license,
    "--test": unittest,
    "--lint": lint,
    "--help": show_help,
}

if __name__ == '__main__':
    for x, f in actions.items():
        if x in "".join(argv): f()

    if not any([x in "".join(argv) for x in actions.keys()]):
        show_help()
