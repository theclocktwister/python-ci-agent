FROM python:latest

RUN apt-get update
RUN apt-get install -y wkhtmltopdf wget curl nano

RUN pip install pytest pytest-html pytest-cov anybadge pylint

WORKDIR /mnt

# these lines can easily be avoided by just usind ADD app/ and
# creating the ourput folder inside the script if it's not existent.
# This would save one extra Docker image layer, thus saving time
# and bandwith on Docker pulls.
RUN mkdir -p /app /output/badges
ADD app/* /app/

RUN chmod +x /app/generator.py && ln -s /app/generator.py /usr/bin/pyci
